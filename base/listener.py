"""OMPD Listener and Server Socket."""

from configuration import HOST, PORT, APPS

import socketserver


class Server(socketserver.BaseRequestHandler):
    """Class to handle all incoming connections."""

    def handle(self):
        """Handle each connection and redirect them to appropriate function."""
        path, data = self._recv_data()
        if path[0] == 0:
            response = data
        elif path[0] in APPS.keys():
            response = APPS[path[0]](data)
        else:
            response = b"Error"
        self._send_data(response)

    def _recv_data(self):
        """Receive the entire msg sent by client and extract from it data."""
        msg = self.request.recv(8)
        msg_length = int(msg.hex(), 16)
        msg += self.request.recv(msg_length + 8)
        self._check_msg_validity(msg)
        path = msg[9:9+msg[8]]
        data = msg[9+msg[8]:9+msg_length-2]
        return path, data

    def _send_data(self, data):
        response = data + b"\x26"
        le = len(data).to_bytes(8, "big")
        response = le + response + le
        self.request.sendall(response)

    @staticmethod
    def _check_msg_validity(msg):
        """
        Check the validity of a message to the OMP.

        However, this method doesn't give a shit about the validity of the
        data, as custom types might be implemented, it just ensure that it has
        a basic method structure.
        """
        pass  # TODO


def start():
    """Start the socket server."""
    with socketserver.ThreadingTCPServer((HOST, PORT), Server) as listener:
        listener.serve_forever()
