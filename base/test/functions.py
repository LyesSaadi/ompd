"""Decorators used to facilitate testing."""

from socketserver import TCPServer
from threading import Thread, Lock

from base.listener import Server


def run_server(HOST, PORT, timeout=10):
    """Run a temporary server for testing (decorator)."""
    def decorator(fun):  # We only accept fun here
        def mod(*args, **kwargs):
            class thread(Thread):  # Not meant to be used elsewhere
                def __init__(self, HOST, PORT):
                    self.HOST = HOST
                    self.PORT = PORT
                    Thread.__init__(self)

                def run(self):
                    with TCPServer((self.HOST, self.PORT), Server) as listener:
                        self.port = listener.server_address[1]
                        port_assigned.release()
                        listener.timeout = timeout
                        listener.handle_request()
            port_assigned = Lock()
            port_assigned.acquire()
            t = thread(HOST, PORT)
            t.start()
            with port_assigned:
                exe = fun(t.port, *args, **kwargs)  # Here, we execute the fun
            t.join()
            return exe
        return mod
    return decorator


def form_request(section, data=b""):
    """Form a valid OMPD Request from a section and it's data."""
    nb_section = len(section).to_bytes(1, "big")
    le = (len(section + data) + 2).to_bytes(8, "big")
    return le + nb_section + section + data + b"\x26" + le


def is_response_data(response, data):
    """Extract data from response and compare it to data."""
    le = response.recv(8)
    received = response.recv(int(le.hex(), 16))
    if not response.recv(9) == b"\x26" + le:
        return False
    elif received == data:
        return True
    else:
        return False
