"""Test suites to verify that the socket is properly working."""

import socket

from base.test.functions import run_server, form_request, is_response_data

# Socket test configuration
HOST = "127.0.0.1"
PORT = 0


@run_server(HOST, PORT, timeout=3)
def test_connection(port):
    """Test to verify the connectivity of the socket."""
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, port))
        data = b"\x00"
        s.sendall(form_request(b"\x00", data))
        assert is_response_data(s, data)


@run_server(HOST, PORT, timeout=10)
def test_send_ridiculous_amount_of_data(port):
    """Test to verify that the socket retrieves everything."""
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, port))
        data = """
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus.
        Suspendisse lectus tortor, dignissim sit amet, adipiscing nec,
        ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula
        massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci
        nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl
        sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae,
        consequat in, pretium a, enim. Pellentesque congue. Ut in risus
        volutpat libero pharetra tempor. Cras vestibulum bibendum augue.
        Praesent egestas leo in pede. Praesent blandit odio eu enim.
        Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum
        primis in faucibus orci luctus et ultrices posuere cubilia Curae;
        Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum.
        Maecenas adipiscing ante non diam sodales hendrerit.

        Ut velit mauris, egestas sed, gravida nec, ornare ut, mi. Aenean ut
        orci vel massa suscipit pulvinar. Nulla sollicitudin. Fusce varius,
        ligula non tempus aliquam, nunc turpis ullamcorper nibh, in tempus
        sapien eros vitae ligula. Pellentesque rhoncus nunc et augue. Integer
        id felis. Curabitur aliquet pellentesque diam. Integer quis metus
        vitae elit lobortis egestas. Lorem ipsum dolor sit amet, consectetuer
        adipiscing elit. Morbi vel erat non mauris convallis vehicula. Nulla
        et sapien. Integer tortor tellus, aliquam faucibus, convallis id,
        congue eu, quam. Mauris ullamcorper felis vitae erat. Proin feugiat,
        augue non elementum posuere, metus purus iaculis lectus, et tristique
        ligula justo vitae magna.

        Aliquam convallis sollicitudin purus. Praesent aliquam, enim at
        fermentum mollis, ligula massa adipiscing nisl, ac euismod nibh nisl
        eu lectus. Fusce vulputate sem at sapien. Vivamus leo. Aliquam euismod
        libero eu enim. Nulla nec felis sed leo placerat imperdiet. Aenean
        suscipit nulla in justo. Suspendisse cursus rutrum augue. Nulla
        tincidunt tincidunt mi. Curabitur iaculis, lorem vel rhoncus faucibus,
        felis magna fermentum augue, et ultricies lacus lorem varius purus.
        Curabitur eu amet.
        """.encode("utf-8")
        # I thought of doing something bigger, but I just said to myself:
        # Let's not just add 60kb of data for nothing...
        # And couldn't do a randomized enormous send of data, cause, you know,
        # when you randomize 1 bit, it takes 1 sec, when you randomize
        # 18446744073709551615, however, it tells you to go fuck yourself.
        s.sendall(form_request(b"\x00", data))
        assert is_response_data(s, data)
