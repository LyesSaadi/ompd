#!/usr/bin/python3
# coding: utf8

"""
Entrypoint for the OMPD.

Hello, this is the file governing them all, for now, it only initiate
the listener in a dumb way. But there's more to come... Maybe...

Functions:
- start(): starting the listener
"""

from base.listener import start

start()  # Starts the server
