"""
Configuration file for the OMPD.

This file is used to configure the OMPD.
You are invited to tweak it as you wish.
Just remember that everything you change here,
will have a direct impact on the way the OMPD
works, so edit this file carefully.

Each variable comes with a docstring like this:
> TODO
"""

# Listener Configuration

"""
Placeholder
"""
HOST = None

"""
Placeholder
"""
PORT = 10205

"""
Placeholder
"""
APPS = {
    1: b"Hello, World!"  # For test purposes only
}
