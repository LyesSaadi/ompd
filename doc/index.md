# The Open Medium Protocol Daemon Documentation

Welcome, traveller in this unknown and still developing world! You're
in the quest of answers, aren't you? Aren't we all in the quest of an
answer for our existence? Don't worry young men, lost in the woods,
this guide, this documentation... is absolutely not going to give you
a philosophical answer about you're existence... don't have time for
that!

But, whether you're travelling alone in this long quest, far far away
from your home (or just lazily sitting in your couch... No I'm not
judging you... Especially since it's basically what I'm doing right
now :3), in order to setup your own TOM[^1] server, or because you ran
into an issue and you'd like to dive deep, or you've decided to help
us out in our own long quest, or because someone told you to and
you just currently want to die because I speak waaay too much, or
because you want to save the Princess. Well, you're in the right place
:D! Except for the last one, go play Mario if you want to save a
princess! And for the one before, go see these videos[^2][^3][^4] and
go back to us when you're in better mood, I don't like sad peoples, I
want happy peoples :)...

## Brief introduction on the Open Medium Protocol

```
┌─────────────────┐
│ ┏━━━━━━━━━━━━━┓ │  ┏━━━━━━━━━━┓   ┏━━━━━━━━━━┓   ┏━━━━━━━━━━┓
│ ┃ Back Ends   ┃ │  ┃ Server 1 ┠───┨ Server 2 ┠───┨ Server 3 ┃ ← You are here
│ ┗━━━━━━━━━━━━━┛ │  ┗━━━━┯━━━━━┛   ┗━━━━┯━━━━━┛   ┗━━━━┯━━━━━┛
├─────────────────┤       │              │              │
│ ┌─────────────┐ │       │      ┌───────┴──────────────┴─────┐
│ │ Middle Mans │ │       ├──────┤ Algorithms: Anon. or pers. │ *Anonymous or personalized
│ └─────────────┘ │       │      └─────────┬────────────┬─────┘
├─────────────────┤       ├────────────┐   │            │
│ ┌─────────────┐ │  ┌────┴─────┐   ┌──┴───┴───┐  ┌─────┴─────┐
│ │ Front Ends  │ │  │ Client 1 │   │ Client 2 │  │ Bob       │
│ └─────────────┘ │  └──────────┘   └──────────┘  └───────────┘
└─────────────────┘
```

> TODO

[^1]: The Open Medium
[^2]: [KITTIES!](https://www.reddit.com/r/cats/) Please find a cute cat videos, this is urgent. We
    must save the world!
[^3]: [DESERT RAIN FROGGIES!](https://youtube.com/watch?v=HBxn56l9WcU)
[^4]: [porn.](https://youtube.com/watch?v=dQw4w9WgXcQ)
