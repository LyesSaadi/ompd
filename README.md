# OMPD: The Open Medium Protocol Daemon

The Open Medium Protocol Daemon is the *official* implementation of
**The Open Medium Protocol** server component written in Python!

## What's The Open Medium Project/Protocol

The Open Medium Project is an open source initiative to create a
comprehensive, modular, decentralised and easy to use social network.

Each component of the Open Medium Project communicate with each other
through the Open Medium Protocol.

To see a more detailed description of how all this work, go look at
the [documentation](/doc/index.md).

## Usage

Just run the main.py file in the main directory with python3. Isn't it
simple?

```bash
python3 main.py
```

This will initiate the listener that will... **listen** (you didn't
expected that, didn't you?) to the specified port in
configuration.py.

## Installation

> TODO

## File Structure

* `base/`: *Here lies dragons*, the basic code used by the whole
  project, where class are defined, and where all the magic
  occur. Unless you know what you're doing, **you shouldn't touch this
  directory at all**, or that might make it incompatible with other
  OMP-compatible software...
* `configuration.py`: Where all the configuration is defined, change
  this directory as you wish. It won't break anything. But, it's not
  the only configuration file of the project:
  ```
  configuration.py
  ~/.config/ompd/configuration.py
  /etc/ompd/configuration.py
  base/configuration.py
  ```
  The program looks to these files from top to bottom, if one is
  found, it stops searching.
* `main.py`: Just the entrypoint to the daemon. Used by dockerfiles,
  systemd services or symlinks to the `ompd` command in PATH.
* `requirements.txt`: All you need ;)!
* `README.md`: *YOU ARE HERE.*

### Optionnal File Structure

This file structure is not present by default, you can create it
yourself, or you can use another one if you want to extend the
project.

* `apps`: Add here your own apps, if you want, no pressure ;).
